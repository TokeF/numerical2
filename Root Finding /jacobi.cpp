#include <iostream>
#include <gsl/gsl_matrix.h>
#include <vector>
#include <functional>
#include <gsl/gsl_blas.h>
#include <cmath>
#include <gsl/gsl_vector_double.h>
#include "QR.h"

void printVector(gsl_vector* v, std::string name) {
    std::cout << "" << std::endl;
    std::cout << name << std::endl;
    for (size_t n = 0; n < v->size; n++) {
        std::cout << gsl_vector_get(v, n) << std::endl;
    }
}

int newtonRoot(const std::vector<std::function<double(gsl_vector* x)>> f, gsl_vector* xstart,
                gsl_vector* fx, double dx, double epsilon, gsl_matrix* J, gsl_matrix* R, gsl_vector* delx){
    int n = f.size(), m = xstart->size, iterations = 0;
    do {
        //calculate fx
        for (int j = 0; j < n; ++j) {
            gsl_vector_set(fx, j, f.at(j)(xstart));
        }
        //calculate J matrix
        for (int i = 0; i < n; ++i) {
            for (int k = 0; k < m; ++k) {
                //calc fi(x1..,x+dx...xn)
                gsl_vector_set(xstart, k, gsl_vector_get(xstart, k) + dx);
                double fidx = f.at(i)(xstart);
                //calc fi(x1..,x...xn)
                gsl_vector_set(xstart, k, gsl_vector_get(xstart, k) - dx);
                double fi = f.at(i)(xstart);
                gsl_matrix_set(J, i, k, (fidx - fi) / dx);
            }
        }
        //calculate delta x
        QR qr;
        qr.qr_gs_decomp(J, R);
        gsl_vector_scale(fx, -1); //-fx
        qr.qr_gs_solve(J,R,fx,delx);
        gsl_vector_scale(fx, -1); //--fx = fx (convert to original fx)

        double fxSqSum;
        double lambda = (double) 1;
        do{
            fxSqSum = 0;
            gsl_vector_scale(delx, lambda);
            gsl_vector_add(xstart, delx);
            for (unsigned int i = 0; i < f.size(); ++i) {
                fxSqSum += pow(f.at(i)(xstart), 2);
            }
            gsl_vector_scale(delx, 1/lambda);
            gsl_vector_scale(delx, -1);
            gsl_vector_add(xstart, delx);
            gsl_vector_scale(delx, -1);
            lambda = lambda/2.f;
        }while (sqrt(fxSqSum) > (1 - lambda/2.f) * gsl_blas_dnrm2(fx) && lambda > 1/64.f);

        gsl_vector_scale(delx, lambda);
        gsl_vector_add(xstart, delx);
        iterations++;
    }while(gsl_blas_dnrm2(fx) > epsilon);

    return iterations;
}

int newtonRoot_knownJacobi(const std::vector<std::function<double(gsl_vector* x)>> f, gsl_vector* xstart,
                            gsl_vector* fx, double epsilon, std::function<double(gsl_vector* x)> J[2][2], gsl_matrix* R,
                            gsl_matrix* Jval, gsl_vector* delx){
    int n = f.size(), m = xstart->size, iterations = 0;
    do {
        //calculate fx
        for (int j = 0; j < n; ++j) {
            gsl_vector_set(fx, j, f.at(j)(xstart));
        }
        //calculate J matrix
        for (int i = 0; i < n; ++i) {
            for (int k = 0; k < m; ++k) {
                gsl_matrix_set(Jval, i, k, J[i][k](xstart));
            }
        }
        //calculate delta x
        QR qr;
        qr.qr_gs_decomp(Jval, R);
        gsl_vector_scale(fx, -1); //-fx
        qr.qr_gs_solve(Jval,R,fx,delx);
        gsl_vector_scale(fx, -1); //--fx = fx (convert to original fx)

        double fxSqSum;
        double lambda = (double) 1;
        do{
            fxSqSum = 0;
            gsl_vector_scale(delx, lambda);
            gsl_vector_add(xstart, delx);
            for (unsigned int i = 0; i < f.size(); ++i) {
                fxSqSum += pow(f.at(i)(xstart), 2);
            }
            gsl_vector_scale(delx, 1/lambda);
            gsl_vector_scale(delx, -1);
            gsl_vector_add(xstart, delx);
            gsl_vector_scale(delx, -1);
            lambda = lambda/2.f;
        }while (sqrt(fxSqSum) > (1 - lambda/2.f) * gsl_blas_dnrm2(fx) && lambda > 1/64.f);

        gsl_vector_scale(delx, lambda);
        gsl_vector_add(xstart, delx);
        iterations ++;
    }while(gsl_blas_dnrm2(fx) > epsilon);

    return iterations;
}


