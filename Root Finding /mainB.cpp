#include <gsl/gsl_vector.h>
#include <functional>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <cmath>
#include <iostream>
//
// Created by toke on 5/24/17.
//
int newtonRoot_knownJacobi(const std::vector<std::function<double(gsl_vector* x)>> f, gsl_vector* xstart,
                            gsl_vector* fx, double epsilon, std::function<double(gsl_vector* x)> J[2][2], gsl_matrix* R,
                            gsl_matrix* Jval, gsl_vector* delx);
void printVector(gsl_vector* v, std::string name);

int main() {
    std::vector<std::function<double(gsl_vector* x)>> funcs;
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 10000* x * y - 1;});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return exp(-x) + exp(-y) - 1 - 1/10000.f;});

    gsl_vector* xstart = gsl_vector_alloc(2);
    gsl_vector* fx = gsl_vector_alloc(funcs.size());
    gsl_vector* delx = gsl_vector_alloc(xstart->size);
    gsl_matrix* Jval = gsl_matrix_alloc(funcs.size(),xstart->size);
    gsl_matrix* R = gsl_matrix_alloc(xstart->size,xstart->size);
    std::function<double(gsl_vector* x)> Jac[2][2];

    //part A1
    gsl_vector_set(xstart,0,9);
    gsl_vector_set(xstart,1,1e-4);

    Jac[0][0] = [] (gsl_vector* xVec){
        double y = gsl_vector_get(xVec, 1);
        return 10000 * y ;};
    Jac[0][1] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0);
        return 10000 * x ;};
    Jac[1][0] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0);
        return -exp(-x);};
    Jac[1][1] = [] (gsl_vector* xVec){
        double y = gsl_vector_get(xVec, 1);
        return -exp(-y);};
    int itt = newtonRoot_knownJacobi(funcs, xstart, fx, 1e-6, Jac, R, Jval, delx);
    printVector(xstart, "Part A1 with supplied jacobi solution");
    std::cout<<"Number of itterations " << itt <<std::endl;

    //Rosen
    funcs.clear();
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return -2*(1-x)-400*(y-x*x);});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 200*(y-x*x);});
    gsl_vector_set(xstart,0,1);
    gsl_vector_set(xstart,1,1);
    Jac[0][0] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 2-400*(y-x*x)+800*x*x;};
    Jac[0][1] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0),y = gsl_vector_get(xVec, 1);
        return -400*x;};
    Jac[1][0] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0),y = gsl_vector_get(xVec, 1);
        return -400*x;};
    Jac[1][1] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return  200;};
    itt = newtonRoot_knownJacobi(funcs, xstart, fx, 1e-6, Jac, R, Jval, delx);
    printVector(xstart, "Rosenbrock with supplied jacobi solution");
    std::cout<<"Number of itterations " << itt <<std::endl;


    //Himmel
    funcs.clear();
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 4*x*(x*x+y-11)+2*(x+y*y-7);});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 2*(x*x+y-11) + 4 * y * (x+y*y-7);});

    gsl_vector_set(xstart,0,5);
    gsl_vector_set(xstart,1,10);
    Jac[0][0] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 2+4*(x*x+y-11)+8*x*x;};
    Jac[0][1] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0),y = gsl_vector_get(xVec, 1);
        return 4*(x+y);};
    Jac[1][0] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0),y = gsl_vector_get(xVec, 1);
        return 4*(x+y);};
    Jac[1][1] = [] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return  2+4*(x+y*y-7)+8*y*y;};
    itt = newtonRoot_knownJacobi(funcs, xstart, fx, 1e-6, Jac, R, Jval, delx);
    printVector(xstart, "Himmelblau with supplied jacobi solution");
    std::cout<<"Number of itterations " << itt <<std::endl;
}
