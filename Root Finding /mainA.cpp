#include <iostream>
#include <gsl/gsl_matrix.h>
#include <vector>
#include <functional>
#include <gsl/gsl_blas.h>
#include <cmath>
#include <gsl/gsl_vector_double.h>
void printVector(gsl_vector* v, std::string name);
int newtonRoot(const std::vector<std::function<double(gsl_vector* x)>> f, gsl_vector* xstart,
                gsl_vector* fx, double dx, double epsilon, gsl_matrix* J, gsl_matrix* R, gsl_vector* delx);

int main() {
    //Part A I
    std::vector<std::function<double(gsl_vector* x)>> funcs;
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 10000* x * y - 1;});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return exp(-x) + exp(-y) - 1 - 1/10000.f;});

    gsl_vector* xstart = gsl_vector_alloc(2);
    gsl_vector* fx = gsl_vector_alloc(funcs.size());
    gsl_vector* delx = gsl_vector_alloc(xstart->size);
    gsl_matrix* J = gsl_matrix_alloc(funcs.size(),xstart->size);
    gsl_matrix* R = gsl_matrix_alloc(J->size2,J->size2);

    gsl_vector_set(xstart,0,9);
    gsl_vector_set(xstart,1,1e-4);
    int itt = newtonRoot(funcs, xstart, fx, 1e-6, 1e-6, J, R, delx);
    printVector(xstart, "Part A1 numerical jacobi solution: ");
    std::cout<<"Number of itterations " << itt <<std::endl;

    //part A II
    gsl_vector_set(xstart,0,3);
    gsl_vector_set(xstart,1,2);
    funcs.clear();
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return -2*(1-x)-400*(y-x*x);});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 200*(y-x*x);});
    itt = newtonRoot(funcs, xstart, fx, 1e-6, 1e-3, J, R, delx);
    printVector(xstart, "Minimum of Rosebrock numerical jacobi solution: ");
    std::cout<<"Number of itterations " << itt <<std::endl;

    //part A III
    gsl_vector_set(xstart,0,5);
    gsl_vector_set(xstart,1,10);
    funcs.clear();
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 4*x*(x*x+y-11)+2*(x+y*y-7);});
    funcs.push_back([] (gsl_vector* xVec){
        double x = gsl_vector_get(xVec, 0), y = gsl_vector_get(xVec, 1);
        return 2*(x*x+y-11) + 4 * y * (x+y*y-7);});
    itt = newtonRoot(funcs, xstart, fx, 1e-6, 1e-3, J, R, delx);
    printVector(xstart, "Minimum of Himmelblau numerical jacobi solution: ");
    std::cout<<"Number of itterations " << itt <<std::endl;
    return 0;
}

