#include <iostream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
using namespace std;

int newton(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        void gradient(gsl_vector* x, gsl_vector* df), /* gradient of the objective function */
        void (*hessian)(gsl_vector* x, gsl_matrix* H),   /* matrix of second derivatives of the objective function */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
);

double frosen(gsl_vector* xvec){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    return (1 - x) * (1 - x) + 100 * (y - x*x) * (y -x*x);
}

void grosen(gsl_vector* xvec, gsl_vector* df){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_vector_set(df, 0, x*(400*x*x + 2) - 400 * x * y - 2);
    gsl_vector_set(df, 1, 200*(y - x*x));
}

void hrosen(gsl_vector* xvec, gsl_matrix* H){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_matrix_set(H, 0, 0, 1200 * x*x - 400 * y + 2);
    gsl_matrix_set(H, 0, 1, -400 * x);
    gsl_matrix_set(H, 1, 0, -400 * x);
    gsl_matrix_set(H, 1, 1, 200);
}

double fhimmel(gsl_vector* xvec){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    return (x*x  + y - 11) * (x*x  + y - 11) + (x + y*y - 7) * (x + y*y - 7);
}

void ghimmel(gsl_vector* xvec, gsl_vector* df){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_vector_set(df, 0, x * (4 * x*x + 4 * y - 42) + 2 * y*y - 14);
    gsl_vector_set(df, 1, 2 * x*x + y * (4 * x + 4 *y*y - 26) - 22);
}

void hhimmel(gsl_vector* xvec, gsl_matrix* H){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_matrix_set(H, 0, 0, 12 * x*x + 4 * y - 42);
    gsl_matrix_set(H, 0, 1, 4 * (x + y));
    gsl_matrix_set(H, 1, 0, 4 * (4 * x + y));
    gsl_matrix_set(H, 1, 1, 4 * (x+ 3 * y * y - 5));
}

int main() {
    gsl_vector* xstart = gsl_vector_alloc(2);
    gsl_vector_set(xstart, 0, 5);
    gsl_vector_set(xstart, 1, 10);
    cout << "Rosenbrock Minimum";
    cout << "Iterations: " << newton(frosen, grosen, hrosen, xstart, 1e-14) << endl<<endl;

    gsl_vector_set(xstart, 0, 5);
    gsl_vector_set(xstart, 1, 10);
    cout << "Himmelblau Minumum";
    cout << "Iterations: " << newton(fhimmel, ghimmel, hhimmel, xstart, 1e-14) << endl;
    return 0;
}