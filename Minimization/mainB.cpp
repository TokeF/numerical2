#include <iostream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>
#include "QR.h"
#include <fstream>

using namespace std;

int newtonBroydenUpdate(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        void gradient(gsl_vector* x, gsl_vector* df), /* gradient of the objective function */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
);

int newtonBroydenUpdateNoGradient(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
);

double frosen(gsl_vector* xvec){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    return (1 - x) * (1 - x) + 100 * (y - x*x) * (y -x*x);
}

void grosen(gsl_vector* xvec, gsl_vector* df){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_vector_set(df, 0, x*(400*x*x + 2) - 400 * x * y - 2);
    gsl_vector_set(df, 1, 200*(y - x*x));
}

double fhimmel(gsl_vector* xvec){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    return (x*x  + y - 11) * (x*x  + y - 11) + (x + y*y - 7) * (x + y*y - 7);
}

void ghimmel(gsl_vector* xvec, gsl_vector* df){
    double x = gsl_vector_get(xvec, 0);
    double y = gsl_vector_get(xvec, 1);
    gsl_vector_set(df, 0, x * (4 * x*x + 4 * y - 42) + 2 * y*y - 14);
    gsl_vector_set(df, 1, 2 * x*x + y * (4 * x + 4 *y*y - 26) - 22);
}

double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = sizeof(t)/sizeof(t[0]);

double expDecay(gsl_vector* x)
{
    double sum = 0;
    double ft;
    for (int i = 0; i < N; i++)
    {
        ft = gsl_vector_get(x, 0) * exp( -t[i] / gsl_vector_get(x, 1) ) + gsl_vector_get(x, 2);
        sum += (ft-y[i])*(ft-y[i])/(e[i]*e[i]);
    }
    return sum;
}

int main() {
    QR qr;
    //calc rosen and himmel with broyden update
    gsl_vector* xstart = gsl_vector_alloc(2);
    gsl_vector_set(xstart, 0, 5);
    gsl_vector_set(xstart, 1, 10);
    cout << "Rosenbrock Minimum with broyden";
    cout << "Iterations: " << newtonBroydenUpdate(frosen, grosen, xstart, 1e-14) << endl<<endl;

    gsl_vector_set(xstart, 0, 5);
    gsl_vector_set(xstart, 1, 10);
    cout << "Himmelblau Minimum with broyden";
    cout << "Iterations: " << newtonBroydenUpdate(fhimmel, ghimmel, xstart, 1e-14) << endl<<endl;

    //do fit
    gsl_vector* xstartFit = gsl_vector_alloc(3);
    gsl_vector_set(xstartFit, 0, 3);
    gsl_vector_set(xstartFit, 1, 3);
    gsl_vector_set(xstartFit, 2, 1);
    cout << "Fit to data"<<endl;
    cout << "Iterations: " << newtonBroydenUpdateNoGradient(expDecay, xstartFit, 1e-14) << endl;
    cout << "Fit result: ";
    qr.printVector(xstartFit, "");

    //write fit data to file for plotting
    ofstream writeFile;
    writeFile.open("plotB.txt");
    double steps = 500;
    double dx = (t[N-1] - t[0])/steps;
    for(double i = t[0]; i <= t[N-1]; i +=dx)
    {
        writeFile << i << " " << gsl_vector_get(xstartFit, 0) * exp(-i / gsl_vector_get(xstartFit, 1)) + gsl_vector_get(xstartFit, 2) << endl;
    }
    writeFile.close();
    return 0;
}


