#include <iostream>
#include <gsl/gsl_matrix.h>
#include "QR.h"
#include <gsl/gsl_blas.h>

using namespace std;

//void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
//void qr_gs_solve(gsl_matrix* A, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

// Calculate gradient nummerically
void numgradient( double (*f)(gsl_vector* x), gsl_vector* xstart, gsl_vector* grad, double dx){
    int n = xstart->size;
    double fx = f(xstart);
    for(int i=0; i<n; i++)
    {
        gsl_vector_set(xstart, i, gsl_vector_get(xstart, i) + dx);
        gsl_vector_set(grad, i, (f(xstart) - fx) / dx);
        gsl_vector_set(xstart, i, gsl_vector_get(xstart, i) - dx);
    }
}

int newtonBroydenUpdateNoGradient(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
){
    //allocate needed variables, vector and matrix
    QR qr;
    double fprevious;
    int n = xstart->size, iterations = 0;
    gsl_vector* step = gsl_vector_alloc(n);
    gsl_vector* grad = gsl_vector_alloc(n);
    gsl_matrix* Hinv = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(Hinv);
    gsl_vector* grads = gsl_vector_alloc(n);
    double dx = 1e-6;
    do {
        iterations++;
        //calc f, gradient and hessian inverse
        double fval = f(xstart);
        fprevious = f(xstart);
        numgradient(f, xstart, grad, dx);
        //solve equation 5 to obtain the new step
        gsl_vector_scale(grad, -1);
        gsl_blas_dgemv(CblasNoTrans, 1, Hinv, grad, 0, step); //H^-1 * grad
        //revert grad
        gsl_vector_scale(grad, -1);

        //backtracking linesearch
        double lambda = 2, fl;
        double alpha = 0.01, armijo = 0;
        double * parmijo = &armijo;
        do {
            lambda /= 2;
            gsl_vector_scale(step, lambda);
            gsl_vector_add(xstart, step);
            fl = f(xstart);
            numgradient(f, xstart, grads, dx);

            if (gsl_blas_dnrm2(step) < 1e-6){gsl_matrix_set_identity(Hinv);} //reset hessian inverse if step diverge

            //revert step and xstart to original values
            gsl_vector_scale(step, -1);
            gsl_vector_add(xstart, step);
            gsl_vector_scale(step, -1 / lambda);

            gsl_blas_ddot(step, grad, parmijo);
        } while (fl > fval + alpha * lambda * armijo);

        //update hessian inverse
        double numerator, denominator;
        double * pnumerator = &numerator, * pdenominator = &denominator;
        gsl_matrix* HinvCpy = gsl_matrix_alloc(n,n);
        gsl_matrix_memcpy(HinvCpy, Hinv);
        gsl_vector* stepCpy = gsl_vector_alloc(n);
        gsl_vector_memcpy(stepCpy, step);
        gsl_vector* placeHolder2 = gsl_vector_alloc(n);

        //calc denominator
        gsl_vector_sub(grads, grad); // f(x+s) - f(x) stored in grads
        gsl_blas_dgemv(CblasTrans, 1, Hinv, grads, 0, placeHolder2);
        gsl_blas_ddot(placeHolder2, step, pdenominator);

        //calc numerator
        gsl_blas_dgemv(CblasNoTrans, 1, Hinv, grads, 0, placeHolder2);
        gsl_vector_sub(step, placeHolder2); //s - H^-1y stored in step
        gsl_blas_ddot(step, stepCpy, pnumerator);
        gsl_matrix_scale(HinvCpy, numerator / denominator);
        gsl_matrix_add(Hinv, HinvCpy);

        //update x
        gsl_vector_scale(stepCpy, lambda);
        gsl_vector_add(xstart, stepCpy);
    }while (f(xstart) != fprevious);
    return iterations;
}

int newtonBroydenUpdate(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        void gradient(gsl_vector* x, gsl_vector* df), /* gradient of the objective function */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
){
    //allocate needed variables, vector and matrix
    QR qr;
    int n = xstart->size, iterations = 0;
    gsl_vector* step = gsl_vector_alloc(n);
    gsl_vector* grad = gsl_vector_alloc(n);
    gsl_matrix* Hinv = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(Hinv);
    gsl_vector* grads = gsl_vector_alloc(n);

    do {
        iterations++;
        //calc f, gradient and hessian inverse
        double fval = f(xstart);
        gradient(xstart, grad);
        //solve equation 5 to obtain the new step
        gsl_vector_scale(grad, -1);
        gsl_blas_dgemv(CblasNoTrans, 1, Hinv, grad, 0, step); //H^-1 * grad
        //revert grad
        gsl_vector_scale(grad, -1);

        //backtracking linesearch
        double lambda = 2, fl;
        double alpha = 0.01, armijo = 0;
        double * parmijo = &armijo;
        do {
            lambda /= 2;
            gsl_vector_scale(step, lambda);
            gsl_vector_add(xstart, step);
            fl = f(xstart);
            gradient(xstart, grads);

            if (gsl_blas_dnrm2(step) < 1e-6){gsl_matrix_set_identity(Hinv);} //reset hessian inverse if step diverge

            //revert step and xstart to original values
            gsl_vector_scale(step, -1);
            gsl_vector_add(xstart, step);
            gsl_vector_scale(step, -1 / lambda);

            gsl_blas_ddot(step, grad, parmijo);
        } while (fl > fval + alpha * lambda * armijo);

        //update hessian inverse
        double numerator, denominator;
        double * pnumerator = &numerator, * pdenominator = &denominator;
        gsl_matrix* HinvCpy = gsl_matrix_alloc(n,n);
        gsl_matrix_memcpy(HinvCpy, Hinv);
        gsl_vector* stepCpy = gsl_vector_alloc(n);
        gsl_vector_memcpy(stepCpy, step);
        gsl_vector* placeHolder2 = gsl_vector_alloc(n);

            //calc denominator
        gsl_vector_sub(grads, grad); // f(x+s) - f(x) stored in grads
        gsl_blas_dgemv(CblasTrans, 1, Hinv, grads, 0, placeHolder2);
        gsl_blas_ddot(placeHolder2, step, pdenominator);

            //calc numerator
        gsl_blas_dgemv(CblasNoTrans, 1, Hinv, grads, 0, placeHolder2);
        gsl_vector_sub(step, placeHolder2); //s - H^-1y stored in step
        gsl_blas_ddot(step, stepCpy, pnumerator);
        gsl_matrix_scale(HinvCpy, numerator / denominator);
        gsl_matrix_add(Hinv, HinvCpy);

        //update x
        gsl_vector_scale(stepCpy, lambda);
        gsl_vector_add(xstart, stepCpy);

    }while (f(xstart) > eps);
    qr.printVector(xstart, "");
    return iterations;
}

int newton(
        double (*f)(gsl_vector* x),                  /* objective function to minimize */
        void gradient(gsl_vector* x, gsl_vector* df), /* gradient of the objective function */
        void (*hessian)(gsl_vector* x, gsl_matrix* H),   /* matrix of second derivatives of the objective function */
        gsl_vector* xstart,                      /* starting point, becomes the latest approximation to the root on exit */
        double eps                          /* accuracy goal, on exit |gradient|<eps */
){
    //allocate needed variables, vector and matrix
    QR qr;
    int n = xstart->size, iterations = 0;
    gsl_matrix* hess = gsl_matrix_alloc(n,n);
    gsl_matrix* R = gsl_matrix_alloc(n,n);
    gsl_vector* step = gsl_vector_alloc(n);
    gsl_vector* grad = gsl_vector_alloc(n);

    do {
        iterations++;
        //calc f, gradient and hessian
        double fval = f(xstart);
        gradient(xstart, grad);
        hessian(xstart, hess);
        //solve equation 5 to obtain the new step
        gsl_vector_scale(grad, -1);
        qr.qr_gs_decomp(hess, R);
        qr.qr_gs_solve(hess, R, grad, step);
            //revert grad
        gsl_vector_scale(grad, -1);

        //backtracking linesearch
        double lambda = 2, fl;
        double alpha = 0.01, armijo = 0;
        double * parmijo = &armijo;
        do {
            lambda /= 2;
            gsl_vector_scale(step, lambda);
            gsl_vector_add(xstart, step);
            fl = f(xstart);

            //revert step and xstart to original values
            gsl_vector_scale(step, -1);
            gsl_vector_add(xstart, step);
            gsl_vector_scale(step, -1 / lambda);

            gsl_blas_ddot(step, grad, parmijo);
        } while (fl > fval + alpha * lambda * armijo);
        //update xstart with lambda * step
        gsl_vector_scale(step, lambda);
        gsl_vector_add(xstart, step);
    }while (f(xstart) > eps);
    qr.printVector(xstart, "");
    return iterations;
}