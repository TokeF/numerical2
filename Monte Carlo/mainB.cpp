#include <iostream>
#include <armadillo>
#include <math.h>
using namespace std;
using namespace arma;

double plainmc(double f(vec x), vec a, vec b, int N, double* err);

double f(vec x){
    return x(0) + x(1) + x(2);
}

int main(){
    vec a, b;
    int N = 10;
    double error;
    double*  perror = &error;

    a ={0,0,0};
    b ={1, 1, 1};
    int itte = 100;
    vec errorList(itte);
    for (int i = 0; i < itte; ++i) {
        plainmc(f, a, b, N, perror);
        cout << N<< " " << error<<endl;
        N += 5;
    }

    return 0;
}