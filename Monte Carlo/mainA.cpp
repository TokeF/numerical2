#include <iostream>
#include <armadillo>
#include <math.h>
using namespace std;
using namespace arma;

double plainmc(double f(vec x), vec a, vec b, int N, double* err);

double f(vec x){
    return x(0) + x(1) + x(2);
}

double testf(vec x) {
    return 1/(1-cos(x(0))*cos(x(1))*cos(x(2)))*1/(M_PI*M_PI*M_PI);
}

int main(){
    vec a = {0, 0, 0}, b = {1, 1, 1};
    int N = 1e6;
    double error, Q;
    double*  perror = &error;

    Q = plainmc(f, a, b, N, perror);
    cout <<"Integrand: x + y + z from a = "<< a.t() <<" to b = "<< b.t();
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 1.5" << endl;
    cout << "" << endl;

    a ={0,0,0};
    b ={M_PI,M_PI,M_PI};
    Q = plainmc(testf, a, b, N, perror);
    cout <<"test integral"<< endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 1.393203929685676859" << endl;
    cout << "" << endl;


    return 0;
}