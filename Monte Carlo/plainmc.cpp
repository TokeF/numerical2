#include <stdlib.h>
#include <math.h>
#include <armadillo>
#define RND ((double)rand()/RAND_MAX)

using namespace arma;

vec randomx(vec a, vec b) {
    int size = a.size();
    vec rx(size);
    for(int i = 0; i < size; i++)
    {
        rx(i) = a(i) + RND*(b(i)-a(i));
    }
    return rx;
}

double plainmc(double f(vec x), vec a, vec b, int N, double* err) {
    double vol=1, sum=0, sum2=0, fx, mean, var;
    int asize = a.size();
    vec rx(asize);
    for(int i=0; i<asize; i++){
        vol*=b[i]-a[i];
    }
    for(int i=0; i<N; i++)
    {
        rx = randomx(a,b);
        fx = f(rx);
        sum += fx;
        sum2 += fx*fx;
    }
    mean = sum/N;
    var = sum2/N-mean*mean;
    *err = sqrt(var/N)*vol;
    return mean*vol;
}
