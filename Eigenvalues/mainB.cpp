#include <iostream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

#define RND ((double)rand()/RAND_MAX)

int jacobi2(gsl_matrix *A, gsl_matrix *V, int eigenvals) {
    int n = A->size1;
    double App, Aqq, Apq, Api, Aqi, Ampp, Amqq, Ampq, Ampi, Amqi;
    int sweeps = 0, es;
    gsl_vector_view e = gsl_matrix_diagonal(A);
    gsl_matrix_set_identity(V);

    if (eigenvals == 0){
        es = n - 1;
    }else {
        es = eigenvals;
    }
    gsl_vector* conv = gsl_vector_alloc(es);
    for (int k = 0; k < es; k++) {
        int p = h;
        do {
            for (int q = p + 1; q < n; q++) {
                App = gsl_matrix_get(A, p, p);
                Aqq = gsl_matrix_get(A, q, q);
                Apq = gsl_matrix_get(A, p, q);
                double phi = 0.5 * atan2(2 * Apq, (Aqq - App));
                double c = cos(phi), s = sin(phi);
                Ampp = c * c * App - 2 * s * c * Apq + s * s * Aqq;
                Amqq = s * s * App + 2 * s * c * Apq + c * c * Aqq;
//                Ampq = s * c * (App - Aqq) + (c * c - s * s) * Apq;
//                gsl_matrix_set(A, p, p, Ampp);
//                gsl_matrix_set(A, q, q, Amqq);
//                gsl_matrix_set(A, p, q, Ampq);
//                gsl_matrix_set(A, q, p, Ampq);
                //iterate trough all columns
                for (int i = 0; i < n; i++) {
                    if (i != p && i != q) {
                        Api = gsl_matrix_get(A, p, i);
                        Aqi = gsl_matrix_get(A, q, i);
                        Ampi = c * Api - s * Aqi;
                        Amqi = s * Api + c * Aqi;
                        gsl_matrix_set(A, p, i, Ampi);
                        gsl_matrix_set(A, q, i, Amqi);
                        gsl_matrix_set(A, i, p, Ampi);
                        gsl_matrix_set(A, i, q, Amqi);
                    }
                }
                for (int i = 0; i < n; i++) {
                    double Vip = gsl_matrix_get(V, i, p);
                    double Viq = gsl_matrix_get(V, i, q);
                    double Vmip = c * Vip - s * Viq;
                    double Vmiq = s * Vip + c * Viq;
                    gsl_matrix_set(V, i, p, Vmip);
                    gsl_matrix_set(V, i, q, Vmiq);
                }
            }
            sweeps += 1;
        } while (App != Ampp);
    }

    return sweeps;
};

int main() {


    return 0;
}