#include <iostream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

#define RND ((double)rand()/RAND_MAX)

void jacobiAlg(gsl_matrix *A, gsl_matrix *V, int p, double* App, double* Ampp) {
    double Aqq, Apq, Api, Aqi, Amqq, Ampq, Ampi, Amqi;
    int n = A->size1;
    for (int q = p + 1; q < n; q++) {
        *App = gsl_matrix_get(A, p, p);
        Aqq = gsl_matrix_get(A, q, q);
        Apq = gsl_matrix_get(A, p, q);
        double phi = 0.5 * atan2(2 * Apq, (Aqq - *App));
        double c = cos(phi), s = sin(phi);
        *Ampp = c * c * *App - 2 * s * c * Apq + s * s * Aqq;
        Amqq = s * s * *App + 2 * s * c * Apq + c * c * Aqq;
        Ampq = s * c * (*App - Aqq) + (c * c - s * s) * Apq;
        gsl_matrix_set(A, p, p, *Ampp);
        gsl_matrix_set(A, q, q, Amqq);
        gsl_matrix_set(A, p, q, Ampq);
        gsl_matrix_set(A, q, p, Ampq);
        //iterate trough all columns
        for (int i = 0; i < n; i++) {
            if (i != p && i != q) {
                Api = gsl_matrix_get(A, p, i);
                Aqi = gsl_matrix_get(A, q, i);
                Ampi = c * Api - s * Aqi;
                Amqi = s * Api + c * Aqi;
                gsl_matrix_set(A, p, i, Ampi);
                gsl_matrix_set(A, q, i, Amqi);
                gsl_matrix_set(A, i, p, Ampi);
                gsl_matrix_set(A, i, q, Amqi);
            }
        }
        for (int i = 0; i < n; i++) {
            double Vip = gsl_matrix_get(V, i, p);
            double Viq = gsl_matrix_get(V, i, q);
            double Vmip = c * Vip - s * Viq;
            double Vmiq = s * Vip + c * Viq;
            gsl_matrix_set(V, i, p, Vmip);
            gsl_matrix_set(V, i, q, Vmiq);
        }
    }

};

int jacobi(gsl_matrix *A, gsl_matrix *V, int p) {
    int n = A->size1;
    double App, Ampp;
    double* pApp = &App, * pAmpp = &Ampp;
    int sweeps = 0;
    do {
        if (p < 0) {
            for (int p = 0; p < (n - 1); p++) {
                jacobiAlg(A, V, p, pApp, pAmpp);
            }
        } else {
            jacobiAlg(A, V, p, pApp, pAmpp);
        }

        sweeps += 1;
    } while (App != Ampp);
    return sweeps;
};


void printMatrix(gsl_matrix *A, std::string name) {
    std::cout << "" << std::endl;
    std::cout << name << std::endl;
    for (size_t n = 0; n < A->size1; n++) {
        std::string row = "";
        for (size_t m = 0; m < A->size2; m++) {
            row += " " + std::to_string(gsl_matrix_get(A, n, m));
        }
        std::cout << row << std::endl;
    }
}

int main() {
    int size = 5;
    gsl_matrix *A = gsl_matrix_alloc(size, size);
    gsl_matrix *Acpy = gsl_matrix_alloc(size, size);
    gsl_matrix *V = gsl_matrix_alloc(size, size);
    gsl_matrix_set_identity(V);
    //Generate random symetric matrix for A
    int m = A->size2;
    for (int j = 0; j < m; ++j) {
        for (int i = 0; i <= j; i++) {
            gsl_matrix_set(A, i, j, RND);
            gsl_matrix_set(A, j, i, gsl_matrix_get(A, i, j));
        }

    }
    gsl_matrix_memcpy(Acpy, A);
    int nrSweeps = 0;
    for (size_t k = 0; k < 4; ++k) {
        nrSweeps += jacobi(A, V, k);
    }
    std::cout << "Number of sweeps: " << nrSweeps << std::endl;
    printMatrix(A, "D matrix");
    printMatrix(V, "V matrix");
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, V, Acpy, 0, A);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, V, 0, Acpy);
    printMatrix(Acpy, "V^T A V");

    return 0;
}
