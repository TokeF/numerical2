#include <iostream>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <functional>
#include <math.h>
#include "QR.h"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector_double.h>

using namespace std;

int covariance(gsl_matrix* A, gsl_matrix* ATAinv){
    QR qr;
    gsl_matrix* ATA = gsl_matrix_alloc(A->size2, A->size2);
    gsl_matrix* R = gsl_matrix_alloc(ATA->size2, ATA->size2);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, A, A, 0, ATA);
    qr.qr_gs_decomp(ATA, R);
    qr.qr_gs_inverse(ATA, R, ATAinv);
    return 0;
}

int leastSquare(gsl_vector* x, gsl_vector* y, gsl_vector* dy,
                const std::vector<std::function<double(double)>> funcs){
    QR qr;
    int n = x->size, m = funcs.size();
    gsl_matrix* R = gsl_matrix_alloc(m,m);
    gsl_matrix* A = gsl_matrix_alloc(n,m);
    gsl_vector* c = gsl_vector_alloc(m);
    gsl_vector* b = gsl_vector_alloc(x->size);
    for (int i = 0; i < n; ++i) {
        gsl_vector_set(b, i, gsl_vector_get(y, i) / gsl_vector_get(dy, i));
        for (int k = 0; k < m; ++k) {
            double Aik = funcs.at(k)(gsl_vector_get(x,i)) / gsl_vector_get(dy,i);
            gsl_matrix_set(A, i, k, Aik);
        }
    }
    qr.qr_gs_decomp(A, R);
    qr.qr_gs_solve(A, R, b, c);

    //calculate covariance matrix
    gsl_matrix* RTRinv = gsl_matrix_alloc(R->size2, R->size2);
    covariance(R, RTRinv);
    gsl_vector_view ck = gsl_matrix_diagonal(RTRinv);

    // Print data from fit
    int steps = 500;
    double dx = ( gsl_vector_get(x, n-1)-gsl_vector_get(x, 0) ) / steps;
    for (double i = gsl_vector_get(x, 0); i <= gsl_vector_get(x, n-1); i +=dx)
    {
        double c0 = gsl_vector_get(c, 0), c1 = gsl_vector_get(c, 1), c2 = gsl_vector_get(c, 2);
        double ck0 = sqrt(gsl_vector_get(&ck.vector, 0)), ck1 = sqrt(gsl_vector_get(&ck.vector, 1));
        double ck2 = sqrt(gsl_vector_get(&ck.vector, 2));
        //fit
        cout << i << " " << c0 + i * c1 + 1 / ( i * c2 );
        //fit + ck
        cout <<" " << (c0 + ck0) + i * ( c1 + ck1 ) + 1 / ( i * ( c2 + ck2) );
        //fit - ck
        cout <<" " << (c0 - ck0) + i * ( c1 - ck1 ) + 1 / ( i * ( c2 - ck2) )<< endl;
    }

    return 0;
}

