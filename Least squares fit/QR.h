//
// Created by toke on 5/8/17.
//

#ifndef LEAST_SQUARES_FIT_QR_H
#define LEAST_SQUARES_FIT_QR_H
class QR{
public:
    void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
    void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);
    void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
    void printMatrix(gsl_matrix* v, std::string name);
    void printVector(gsl_vector* v, std::string name);
};
#endif //LEAST_SQUARES_FIT_QR_H
