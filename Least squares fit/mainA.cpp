#include <iostream>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <functional>
#include <gsl/gsl_vector_double.h>
#include "QR.h"
#include <gsl/gsl_blas.h>

using namespace std;

int leastSquare(gsl_vector* x, gsl_vector* y, gsl_vector* dy,
                const std::vector<std::function<double(double)>> funcs);

int main() {
    int size = 10;
    gsl_vector* x = gsl_vector_alloc(size);
    gsl_vector* y = gsl_vector_alloc(size);
    gsl_vector* dy = gsl_vector_alloc(size);
    double xx[size] = {0.100, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372, 1.995, 2.900};
    double yy[size] = {12.644, 9.235, 7.377, 6.460, 5.555, 5.896, 5.673, 6.964, 8.896, 11.355};
    double dyy[size] = {0.858, 0.359, 0.505, 0.403, 0.683, 0.605, 0.856, 0.351, 1.083, 1.002};
    for (int i = 0; i < size; ++i) {
        gsl_vector_set(x, i, xx[i]);
        gsl_vector_set(y, i, yy[i]);
        gsl_vector_set(dy, i, dyy[i]);
    }
    std::vector<std::function<double(double)>> fitFunc;
    fitFunc.push_back([](double x){return 1;});
    fitFunc.push_back([](double x){return x;});
    fitFunc.push_back([](double x){return 1/x;});

    leastSquare(x,y,dy,fitFunc);

    return 0;
}
