#include <armadillo>
#include <deque>

using namespace arma;
using namespace std;
void rkstep12(
        deque<double> t,                                  /* the current value of the variable */
        double h,                                  /* the step to be taken */
        deque<vec>& y,                                 /* the current value y(t) of the sought function */
        vec f(double t, vec& yf), /* the right-hand-side, dydt = f(t,y) */
        vec& yh,                                /* output: y(t+h) */
        vec& err                                /* output: error estimate dy */
){
    int n = y.back().n_rows;
    double tstep = t.front();
    vec k0(n), k05(n);
    k0 = f(tstep, y.front());
    tstep = t.front() + 0.5 * h;
    yh = y.front() + 0.5 * h * k0;
    k05 = f(tstep, yh);
    yh = y.front() + h * k05;
    err = (k0 - k05)*h/2;
}

void driver(
        deque<double>& t,                             /* the current value of the variable */
        double b,                              /* the end-point of the integration */
        double& h,                             /* the current step-size */
        deque<vec>& y,                              /* the current y(t) */
        double acc,                            /* absolute accuracy goal */
        double eps,                            /* relative accuracy goal */
        vec f(double t, vec& yf) /* right-hand-side */
){
    int n = y.size();
    double t0 = t.back(), errNorm;
    vec yh(n), err(n);
    while (t.front() < b){
        if (t.front() + h > b){h = b - t.front();}
        rkstep12(t, h, y, f, yh, err);
        errNorm = norm(err);
        double tol = (eps * norm(yh) + acc) * sqrt(h / (b - t0));
        if (errNorm < tol) //do step
        {
            t.push_front(t.front() + h);
            y.push_front(yh);
        }
        if (errNorm > 0) {
            h *= pow(tol/errNorm,0.25)*0.95 ;
        }
        else {
            h = 2*h;
        }

    }

}