#include <armadillo>
#include <deque>
#include <math.h>
using namespace arma;
using namespace std;

void driver(
        deque<double>& t,                             /* the current value of the variable */
        double b,                              /* the end-point of the integration */
        double& h,                             /* the current step-size */
        deque<vec>& y,                              /* the current y(t) */
        double acc,                            /* absolute accuracy goal */
        double eps,                            /* relative accuracy goal */
        vec f(double t, vec& yf) /* right-hand-side */
);

vec f(double t, vec& yf) {
    vec dydt(yf.size());
    dydt(0) = yf(1);
    dydt(1) = - yf(0);
    return dydt;
}

int main() {
    double b = 2*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
    deque<double> t;
    deque<vec> ydeq;
    vec y(2);
    t.push_front(0);
    y = {1, 1};
    ydeq.push_front(y);
    driver(t, b, h, ydeq, acc, eps, f);
    cout << "Starting at t ="<< t.back()<<" With y = [1 1]" <<endl;
    cout<< "End point is b = "<< b<<endl;
    cout<< "In the end t is " <<t.front()<< " As it should be" << endl<< endl;
    cout << "t and y are stored in a stack. y is printed below: "<<endl<<endl;
    deque<vec >::iterator it = ydeq.begin();

    while (it != ydeq.end()) {
        std::cout << ' ' << *it++<<endl;
    }
    return 0;
}