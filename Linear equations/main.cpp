#include <iostream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <vector>
#include <gsl/gsl_vector_double.h>

#define RND ((double)rand()/RAND_MAX)


void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* A, gsl_matrix* R, gsl_vector* b);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);
void printMatrix(gsl_matrix* A, std::string name);
void printVector(gsl_vector* v, std::string name);

int main() {
    //Matrix QR decomposition
    gsl_matrix* A = gsl_matrix_alloc(5,3);
    gsl_matrix* R = gsl_matrix_calloc(A->size2,A->size2);
    for (size_t i = 0; i < A->size1 ; i++) {
        for (size_t j = 0; j < A->size2; j++) {
            gsl_matrix_set(A,i,j,RND);
        }
    }
    std::cout<<"part A1 "<<std::endl;
    printMatrix(A, "A init");
    qr_gs_decomp(A,R);



    //Random square matrix A2 for inverse and solve of linear equations
    gsl_matrix* A2 = gsl_matrix_alloc(2,2);
    gsl_matrix* originalA2 = gsl_matrix_alloc(2,2);
    gsl_matrix* A2copy = gsl_matrix_alloc(A2->size1,A2->size2);
    gsl_matrix_memcpy(A2copy,A2);
    gsl_matrix* R2 = gsl_matrix_calloc(A2->size2,A2->size2);
    gsl_matrix* B = gsl_matrix_alloc(A2->size1,A2->size2);
    for (size_t i = 0; i < A2->size1 ; i++) {
        for (size_t j = 0; j < A2->size2; j++) {
            gsl_matrix_set(A2,i,j,RND);
        }
    }
    gsl_matrix_memcpy(originalA2,A2);
    std::cout<<""<<std::endl;
    std::cout<<"part A2 "<<std::endl;
    printMatrix(A2, "A2 init");

    //Decomposition
    qr_gs_decomp(A2,R2);
    //Solve system of linear equations using Q and R from previous decomposition of A
    gsl_vector* b = gsl_vector_calloc(A2->size1);
    for (size_t k = 0; k < b->size; k++) {
        gsl_vector_set(b,k,RND);
    }
    printVector(b, "b original");
    qr_gs_solve(A2, R2, b);
    gsl_vector* result = gsl_vector_alloc(b->size);
    gsl_blas_dgemv(CblasNoTrans, 1, originalA2, b, 0, result);
    printVector(result, "A * x =");

    //calculate inverse of A2
    std::cout<<""<<std::endl;
    std::cout<<"part B "<<std::endl;
    qr_gs_inverse(A2, R2, B);

    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, originalA2, B, 0, A2);
    printMatrix(A2, "result of A*B = ");

    return 0;
}

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
    gsl_matrix_set_identity(B);
    for (size_t i = 0; i < B->size2; i++) {
        gsl_vector_view e = gsl_matrix_column(B, i);
        qr_gs_solve(Q, R, &e.vector);
    }
}

void qr_gs_solve(gsl_matrix* A, gsl_matrix* R, gsl_vector* b){
    gsl_vector* x = gsl_vector_calloc(A->size2) ; //Allocate space for solution vector x
    gsl_vector* result = gsl_vector_alloc(b->size);
    gsl_matrix* originalA = gsl_matrix_alloc(A->size1,A->size2);
    gsl_blas_dgemv(CblasTrans, 1, A, b, 0, x); //Q^T*b -> x
    //Backwards substitution. s is used to calculate the substituted constants
    for (int i = R->size1 - 1; i >= 0 ; i--) {
        double s = 0;
        for (size_t k = i + 1; k < R->size2; k++) {
            s += gsl_matrix_get(R, i, k) * gsl_vector_get(x, k);//R(i,k)*x(k);
        }
//        solve the equation x = (Q^T(i)*b(i) - s )/R(i,i)
        gsl_vector_set(x, i, gsl_vector_get(x, i) - s);
        gsl_vector_set(x, i, gsl_vector_get(x, i) / gsl_matrix_get(R, i, i));
    }
    //Ony used if A is square, in order to find A inverse.
    if (b->size == x->size) {
        gsl_vector_memcpy(b, x);
    }
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, R, 0, originalA);
    gsl_blas_dgemv(CblasNoTrans, 1, originalA, x, 0, result);
//    printVector(result, "A * x =");
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
    //Allocate memeory for Q^T, and a dummy vector used to hold copy of qi
    //and result matrix, to hold Q * R = A
    gsl_vector *dummy = gsl_vector_alloc(A->size1);
    gsl_matrix* QtransQ = gsl_matrix_calloc(A->size2,A->size2);
    gsl_matrix* result = gsl_matrix_calloc(A->size1,A->size2);

    for (size_t i = 0; i < A->size2; i++) {
        //Get a column from A
        gsl_vector_view qi = gsl_matrix_column(A, i);
        //calculate norm of each column and save in R diagonal
        double norm = gsl_blas_dnrm2(&qi.vector);
        gsl_matrix_set(R, i, i, norm);
        //calculate and save q_i in place
        gsl_vector_scale(&qi.vector, 1/norm);
        //calculate remaining part of Q, ie. a_j
        for (size_t j = i+1; j < A->size2; j++) {
            double sclProduct = 0;
            double *sclPoint = &sclProduct;
            gsl_vector_view aj = gsl_matrix_column(A, j);
            gsl_blas_ddot(&qi.vector, &aj.vector, sclPoint); //save qi*aj in sclProduct
            gsl_matrix_set(R,i,j, *sclPoint); //Rij is set to qi*aj

            gsl_vector_memcpy(dummy, &qi.vector);

            gsl_vector_scale(dummy, *sclPoint); //calc qi * Rij, and save in dummy

            gsl_vector_sub(&aj.vector, dummy); //calc aj - qi * Rij and save in aj

        }
    }

    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, A, A, 0, QtransQ);

    printMatrix(R, "R");

    printMatrix(QtransQ, "Q^T*Q");

    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, R, 0, result);

    printMatrix(result, "Q*R");
}

void printMatrix(gsl_matrix* A, std::string name){
    std::cout<<""<<std::endl;
    std::cout<<name<<std::endl;
    for (size_t n = 0; n < A->size1; n++) {
        std::string row = "";
        for (size_t m = 0; m < A->size2; m++) {
            row +=" " + std::to_string(gsl_matrix_get(A,n,m));
        }
        std::cout<<row<<std::endl;
    }
}

void printVector(gsl_vector* v, std::string name){
    std::cout<<""<<std::endl;
    std::cout<<name<<std::endl;
    for (size_t n = 0; n < v->size; n++) {
        std::cout<<gsl_vector_get(v,n)<<std::endl;
    }
}

