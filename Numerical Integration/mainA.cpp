#include <math.h>
#include <iostream>
#include <functional>

using namespace std;
double integrator(function<double(double)> f, double a, double b, double eps, double acc, double *error, int* nrec);
int main() {
    function<double(double)> f = [](double x){return sqrt(x);};
    function<double(double)> f1 = [](double x){return 1 / sqrt(x);};
    function<double(double)> f2 = [](double x){return log(x)/ sqrt(x);};
    function<double(double)> f3 = [](double x){return 4 * sqrt(1 - pow((1 - x),2));};

    int  nrec = 0;
    int* pnrec = &nrec;
    double a = 0, b = 1, eps = 0.001, acc = 0.001, error = 0, Q;
    double* perror = &error;

    Q = integrator(f, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: sqrt(x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 2/3" << endl;
    cout << "" << endl;

    Q = integrator(f1, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: 1 / sqrt(x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 2" << endl;
    cout << "" << endl;

    Q = integrator(f2, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: log(x)/ sqrt(x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = -4" << endl;
    cout << "" << endl;

    Q = integrator(f3, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: 4 * sqrt(1 - pow((1 - x),2)) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = pi" << endl;
    cout << "" << endl;
    return 0;
}