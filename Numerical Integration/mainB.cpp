#include <math.h>
#include <iostream>
#include <functional>

using namespace std;

double integrator(function<double(double)> f, double a, double b, double acc, double eps, double *error, int* nrec);

int main() {
    function<double(double)> f = [](double x){return 1 / cosh(x);};
    function<double(double)> f1 = [](double x){return sqrt(x) * exp(-x);};
    function<double(double)> f2 = [](double x){return 1/(x*x + 2*2);};

    int  nrec = 0;
    int* pnrec = &nrec;
    double a = -INFINITY, b = INFINITY, eps = 0.001, acc = 0.001, error = 0, Q;
    double* perror = &error;

    Q = integrator(f, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: 1 / cosh(x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = pi" << endl;
    cout << "" << endl;

    a = 0, b = INFINITY;
    Q = integrator(f1, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: sqrt(x) * exp(-x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 0.886226" << endl;
    cout << "" << endl;

    a = -INFINITY, b = 0;
    Q = integrator(f2, a, b, eps, acc, perror, pnrec);
    cout <<"Integrand: sqrt(x) * exp(-x) from a = "<< a <<" to b = "<< b << ". Recursions: "<< nrec <<endl;
    cout << "Q = " << Q << " With error = " << error << " Should be Q = 0.785398" << endl;
    cout << "" << endl;

    return 0;
}