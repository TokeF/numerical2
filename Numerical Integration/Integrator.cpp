#include <math.h>
#include <assert.h>
#include <functional>
#include <iostream>
using namespace std;

double adapt24(function<double(double)> f, double a, double b, double eps,
               double acc, double* error, double f2, double f3, int* nrec) {
    assert(*nrec < 1e6); //avoid inf recursion
    //Weight and values are calculated as per equation 51
    double f1 = f(a+(b-a)/6);
    double f4 = f(a+(b-a)*5/6);

    //Higher order estimate
    double Q = (2*f1 + f2 + f3 + 2*f4) / 6 * (b-a);

    //Lower order estimate
    double q = (f1 + f2 + f3 + f4) / 4 * (b-a);
    *error = fabs(Q - q);
    double tol = acc + eps * fabs(Q);
    if (*error < tol){*nrec = *nrec + 1; return Q;}
    else{
        //integrate two half subintervals
        *nrec = *nrec + 1;
        double Q1 = adapt24(f, a, (a+b)/2, eps, acc / sqrt(2), error, f1, f2, nrec);
        *nrec = *nrec + 1;
        double Q2 = adapt24(f, (a+b)/2, b, eps, acc / sqrt(2), error, f3, f4, nrec);
        return Q1 + Q2;
    }
}

double integrator(function<double(double)> f, double a, double b, double eps, double acc, double *error, int* nrec)
{
    double f2 = f(a + (b - a) * 2 / 6);
    double f3 = f(a + (b - a) * 4 / 6);

    if (isinf(a) && isinf(b)){
        function<double(double)> fsub =
                [f](double x){return f(x/(1-x*x))*(1+x*x)/((1-x*x)*(1-x*x));};
        a = -1; b = 1;
        double f2 = fsub(a+(b-a)*2/6);
        double f3 = fsub(a+(b-a)*4/6);
        return adapt24(fsub, a, b, eps, acc, error, f2, f3, nrec);
    }else if (isinf(a) && !isinf(b)) {
        function<double(double)> fsub = [f,b](double x){return f(b-(1-x)/x)/x/x;};
        a = 0; b = 1;
        double f2 = fsub(a+(b-a)*2/6);
        double f3 = fsub(a+(b-a)*4/6);
        return adapt24(fsub, a, b, eps, acc, error, f2, f3, nrec);
    } else if (!isinf(a) && isinf(b)) {
        function<double(double)> fsub = [f, a](double x) { return f(a + (1 - x) / x) / x / x; };
        a = 0;
        b = 1;
        double f2 = fsub(a + (b - a) * 2 / 6);
        double f3 = fsub(a + (b - a) * 4 / 6);
        return adapt24(fsub, a, b, eps, acc, error, f2, f3, nrec);
    }
    return adapt24(f, a, b, eps, acc, error, f2, f3, nrec);
}
