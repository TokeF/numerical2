//
// Created by toke on 4/9/17.
//

#include <math.h>
#include <assert.h>
double linterp(int n, double *x, double *y, double z);

double linterp_integ(int n, double *x, double *y, double z){
    assert(n > 1 && z > x[0] && z <= x[n-1]);
//    double integral = 0;
//
//    int i;
//    for (i = 0; i < ceil(z); i++) {
//        integral += (x[i+1] - x[i]) * (y[i] + (y[i+1] - y[i]) / 2);
//    }
//    double zy = linterp(n, x, y, z);
//    double zPart = (z - x[i]) * (y[i] + (zy - y[i]) / 2);
//    return integral + zPart;
    int i = 0;
    int j = n - 1;
    /*binary search*/
    while(j - i > 1){
        int m = (i+j) / 2;
        if(z > x[m]) i = m;
        else j = m;
    }

    return x[i] * y[i]  + (y[i+1] - y[i]) * log(x[i+1] - x[i]) * x[i] * (z - x[i]/2);
};
