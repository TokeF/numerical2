
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

double linterp(int n, double *x, double *y, double z);
double linterp_integ(int n, double *x, double *y, double z);
class qspline;

int main() {
    int size = 60;
    double x[size];
    double y[size];

    for(int i = 0; i<size; i++) {
        x[i] = 3*i-30;
        y[i] = pow(x[i],2);
    }


    double j = -5;
    while (j < 10) {
        cout<<j<<" "<<linterp(size, x, y, j)<<endl;
        j += 0.5;
    }

    cout<<""<<endl;

    j = -3;
    while (j < 5) {
        cout<<j<<" "<<linterp_integ(size, x, y, j)<<endl;
        j += 0.5;
    }

}