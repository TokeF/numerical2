
#include <assert.h>

double linterp(int n, double *x, double *y, double z){
    //assert input makes sense
    assert(n > 1 && z >= x[0] && z <= x[n-1]);
    int i = 0;
    int j = n - 1;
    /*binary search*/
    while(j - i > 1){
        int m = (i+j) / 2;
        if(z > x[m]) i = m;
        else j = m;
    }
    double pi = (y[i+1] - y[i]) / (x[i+1] - x[i]);
    return y[i] + pi * (z - x[i]);
}
