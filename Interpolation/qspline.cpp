//
// Created by toke on 4/10/17.
//
#include <vector>
#include <assert.h>
#include <math.h>
#include <iostream>

using namespace std;

class qspline{
    vector<double> x,y,b,c;
public:
    qspline(vector<double>&x, vector<double>&y);
    double eval(double z), deriv(double z), integ(double z);
};

qspline::qspline(vector<double>& xs, vector<double>& ys)
        : x(xs), y(ys), b(x.size()-1), c(x.size()-1) {
    //save delta x_i in local variable h.
    //calculate linear coefficient p.
    int n=x.size();
    vector<double> h(n-1),p(n-1);
    for(int i=0;i<n-1;i++){
        h[i]=x[i+1]-x[i];
        p[i]=(y[i+1]-y[i])/h[i];
    }

    c[0]=0; // recursion up
    for(int i=0;i<n-2;i++) {
        c[i+1]=(p[i+1]-p[i]-c[i]*h[i])/h[i+1];
    };
    c[n-2]/=2; //recursion down
    for(int i=n-3;i>=0;i--) {
        c[i]=(p[i+1]-p[i]-c[i+1]*h[i+1])/h[i];
    };
    for(int i=0;i<n-1;i++) {
        b[i]=p[i]-c[i]*h[i];
    };

}

double qspline::eval(double z){
    assert(z >= x[0] && z <= x[x.size()-1]);
    int i=0, j=x.size()-1;
    //binary search
    while(j-i>1){
        int m=(i+j)/2;
        if(z>x[m]) i=m;
        else j=m;
    }
    double h=z-x[i];
    return y[i]+h*(b[i]+h*c[i]);
}

double qspline::deriv(double z) {
    int i=0, j=x.size()-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>x[m]) i=m;
        else j=m;
    }
    double h=z-x[i];
    return b[i]+2*h*c[i];}

double qspline::integ(double z)
{
    int i=0, j=x.size()-1;
    while(j-i>1){int m=(i+j)/2; if(z>x[m]) i=m; else j=m;}
    double S=0, h;
    for(int k=0;k<i;k++){
        h=x[k+1]-x[k];
        S += h*(y[k]+h*(b[k]/2+h*c[k]/3));
    }
    h=z-x[i];
    return S + h*(y[i]+h*(b[i]/2+h*c[i]/3));
}

int main(){
    int size=60;
    vector<double> x(size), y(size);
    for(int i = 0; i<size; i++) {
        x[i] = 3*i-30;
        y[i] = pow(x[i],2);
    }

    class qspline qs = qspline(x, y);

    double j = -5;
    while (j < 10) {
        cout<< j <<" "<< qs.eval(j) <<endl;
        j += 0.5;
    }

    cout<<""<<endl;

    j = -5;
    while (j < 10) {
        cout<< j <<" "<< qs.deriv(j) <<endl;
        j += 0.5;
    }

    cout<<""<<endl;

    j = -5;
    while (j < 10) {
        cout<< j <<" "<< qs.integ(j) - 9000 <<endl;
        j += 0.5;
    }

}


