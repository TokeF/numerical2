Hello, world!
This project is a solution to exercise 4:
Bi-linear interpolation on a rectilinear grid in two dimensions

Part A: 
Implementation of bilinear interpolation.
Simple test on 45 degree angled plane in a unitsquare x=[0 1], y = [0 1], F=[(0 0), (1 1)]
and p = [0.5 0.5]

Part B
A semi random x and y vector is created, the F values are calculated with the Rosenbrock
valley function, and a point somewhere in the regien is interpolated. The rectilinear grid
along with the interpolated point is shown on a plot, to see that the point is correct with respect to the grid.

Part C
A hyperbolic paraboloid F(x,y) = x*y is interpolated over the region x = [-1 1], y = [-1 1]
A plot of the interpolated surface is hown, and found to take the correct shape
