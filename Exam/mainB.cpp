#include <iostream>
#include <armadillo>
#include <iostream>
#include <fstream>
using namespace arma;
using namespace std;

double bilinear(vec& x, vec& y, mat& F, double px, double py);

int main() {
    //Define some x and y
    vec x = {-1.5, -1, 0, 0.1, 0.2, 0.5, 0.9, 1.1, 0.7, 1.5, 2, 3};
    vec y = {-1.5, -1.2, -1, -0.7, 0.1, 0.8, 1.4, 2.2, 2.5, 3};
    int n = x.n_rows, m = y.n_rows;
    mat F(n,m);

    ofstream myfile;
    myfile.open("dataB.txt");
    myfile<<1<<" ";
    for (int k = 0; k < m; ++k) {
        myfile << y(k) << " ";
    }
    myfile<<""<<endl;
    for (int i = 0; i < n; ++i) {
        myfile << x(i)<< " ";
        for (int j = 0; j < m; ++j) {
            //rosenbrock valley function
            F(i,j) = (1 - x(i)) * (1 - x(i)) + 100 * (y(j) - x(i)*x(i)) * (y(j) - x(i)*x(i));
            myfile<< F(i,j) << " ";
        }
        myfile <<""<<endl;
    }
    myfile.close();
    double px = 2, py = 2.5;
    double fp = bilinear(x, y, F, px, py);
    cout << px << " " << py << " " <<fp <<endl;

    return 0;
}