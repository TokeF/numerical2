#include <iostream>
#include <armadillo>
using namespace arma;
using namespace std;

double bilinear(vec& x, vec& y, mat& F, double px, double py);

int main() {

    vec x = {0, 1};
    vec y = {0, 1};
    mat F(3,3);
    F = {{0, 0}, {1, 1}};
    double px = 0.5, py = 0.5;
    double fp = bilinear(x, y, F, px, py);
    cout<< "rectiliniar grid is a 45 deg plane"<<endl;
    cout << "Interpolation at point p = [" << px << "," << py << "] yield" << endl;
    cout << fp <<endl;
    cout << "expected 0.5"<<endl;

    return 0;
}