#include <iostream>
#include <armadillo>
#include <iostream>
#include <fstream>
using namespace arma;
using namespace std;

double bilinear(vec& x, vec& y, mat& F, double px, double py);

int main() {
    //Define some x and y
    vec x = {-1, 1};
    vec y = {-1, 1};
    int n = x.n_rows, m = y.n_rows;
    mat F(n,m);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            F(i,j) = x(i)*y(j);
        }
    }

    ofstream myfile;
    myfile.open("dataC.txt");
    double dx = 0.1;
    for (double k = -1+dx; k < 1; k += dx) {
        for (double i = -1+dx; i < 1; i += dx) {
            myfile << bilinear(x, y, F, k, i) << " ";
        }
        myfile <<""<<endl;
    }
    myfile.close();
    return 0;
}

//set pm3d map
//        set xrange [0:2]
//set yrange [0:2]
//set cbrange [0:3]
//set palette rgbformulae 22,13,10
//splot "out.C.txt"