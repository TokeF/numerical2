#include <armadillo>
#include <assert.h>
using namespace arma;
using namespace std;

double bilinear(vec& x, vec& y, mat& F, double px, double py) {
    //Assert input is valid
    assert(x.n_rows > 1 && y.n_rows > 1);
    assert(px > x(0) && px < x(x.n_rows - 1));
    assert(py > y(0) && py < y(y.n_rows - 1));

    int start = 0;
    int end = x.n_rows - 1;
    /*binary search along x*/
    while(end - start > 1){
        int m = (start + end) / 2;
        if(px > x[m]) start = m;
        else end = m;
    }
    double x1 = x(start), x2 = x(end);
    int i = start;

    /*binary search along y*/
    while(end - start > 1){
        int m = (start + end) / 2;
        if(px > x[m]) start = m;
        else end = m;
    }
    start = 0;
    end = y.n_rows - 1;
    double y1 = y(start), y2 = y(end);
    int j = start;

    //Solve equations analytically
    double a0, a1, a2, a3;
    double delx = x2 - x1, dely = y2 - y1;
    a0 = F(i, j);
    a1 = (F(i + 1,j) - F(i,j)) / delx;
    a2 = (F(i,j + 1) - F(i,j)) / dely;
    a3 = (F(i+1,j+1) - F(i,j) - a1 * delx - a2 * dely) / (delx * dely);
    //cout << a0 << " "<< a1 << " "<< a2 << " "<< a3 << " "<<endl;
    double fp = a0 + a1 * (px - x1) + a2 * (py - y1) + a3 * (px - x1) * (py - y1);
    return fp;
}
